<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận đăng ký</title>
    <link rel="stylesheet" href="./style2.css">
</head>
<body>
        <div class="form_container">
        <?php
            include "database.php";

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $name = $_POST["username"];
                $selectedGioiTinh = $_POST["gioiTinh"];
                $selectedGioiTinhString = implode(", ", $selectedGioiTinh);
                $selectedKhoa = $_POST["khoa"];
                $dob = $_POST["dob"];
                $address = $_POST["address"];
                $image = $_POST["image"];

                echo "<p><strong>Họ và Tên:</strong> $name</p>";
                echo "<p><strong>Giới tính:</strong> $selectedGioiTinhString</p>";
                echo "<p><strong>Phân khoa:</strong> $selectedKhoa</p>";
                echo "<p><strong>Ngày sinh:</strong> $dob</p>";
                echo "<p><strong>Địa chỉ:</strong> $address</p>";
                echo "<p><strong>Hình ảnh:</strong> $image</p>";

                // Convert date format to YYYY-MM-DD
                $date = DateTime::createFromFormat('d/m/Y', $dob);
                $dateOfBirth = $date->format('Y-m-d');

                // Sử dụng prepared statement để chèn dữ liệu vào bảng students
                $stmt = $conn->prepare("INSERT INTO students (name, gender, department, birthday, address, image) VALUES (?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("ssssss", $name, $selectedGioiTinhString, $selectedKhoa, $dateOfBirth, $address, $image);

                if ($stmt->execute()) {
                    $registrationSuccess = true;
                } else {
                    echo "Lỗi: " . $stmt->error;
                }

                $stmt->close();
            }

            $conn->close();
        ?>
            </div>
            <br>
            <div class="button-container">
               

                <button type="button" id="confirm-button" onclick="showMessage()">Xác nhận</button>
            </div>
            
           
           



</body>
</html>