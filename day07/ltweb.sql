CREATE DATABASE IF NOT EXISTS ltweb;

USE ltweb;

CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(200) NOT NULl,
    gender ENUM('Nam', 'Nữ') NOT NULL,
    department VARCHAR(50) NOT NULL,
    birthday DATE NOT NULL,
    address TEXT,
    image VARCHAR(300)
);