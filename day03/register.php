<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style1.css">
    <title>Đăng ký Tân sinh viên</title>
</head>

<body>
    <form action="register1.php" class="bd-blue">
        <table class="w-100">

            <tr>
                <td class="label"><label for="name">Họ và tên</label></td>
                <td class="bd-blue">
                    <?php
                    $nameValue = ''; // Giá trị hiện tại của trường nhập liệu
                    if (isset($_POST['name'])) {
                        $nameValue = $_POST['name'];
                    }
                    ?>
                    <input type="text" id="name" name="name" required class=" name-label" value="<?php echo htmlspecialchars($nameValue); ?>">
                </td>
            </tr>

            <tr>
                <td class="label"><label for="gender">Giới tính</label></td>
                <td>
                    <div class="d-flex">
                        <?php
                        $values = array(0 => 'Nam', 1 => 'Nữ');
                        foreach ($values as $key => $value) {
                            $genderClass = ($key == 0) ? 'men' : 'women';
                            $inputId = "gender-$key";

                            echo "<div class='genders'>
                                    <input id='$inputId' type='radio' name='genders' value='$key'>
                                    <label class='no-border no-background' for='$inputId'><span class='gender-label'>$value</span></label>
                                </div>";
                        }
                        ?>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <label for="department">Phân khoa:</label>
                </td>
                <td>
                    <select id="department" name="department" class="bd-blue py-10">
                    <option value="">--Chọn phân khoa--</option>
                        <?php
                            $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                                foreach ($departments as $key => $value) :
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php
                                endforeach;
                                ?>
                        </select>
                </td>
            </tr>

        </table>
        <div class="text-center ">
            <button type="submit" class="btn">Đăng ký</button>
        </div>
    </form>
</body>

</html>