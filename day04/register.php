<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký tân sinh viên</title>
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
    
    <div>
        <h1></h1>
        <form method="POST" id="registration-form" >

        <div id="error-container"></div>

            <label class="username_label" for="username">Họ và tên</label>
            <input type="text" id="username" name="username" class="username">

            <br><br>

            <div class="container">
                <label class="username_label" for="checkbox">Giới tính</label>
                <div class="form-checkbox" id="checkbox">
                    <label for="checkbox_1" class="checkbox-label">
                        <input type="checkbox" class="checkbox" id="checkbox_1" name="gioiTinh[]" value="Nam" />
                        <span class="checkbox-custom"></span>
                        Nam
                    </label>

                    <label for="checkbox_2" class="checkbox-label">
                        <input type="checkbox" class="checkbox" id="checkbox_2" name="gioiTinh[]" value="Nữ" />
                        <span class="checkbox-custom"></span>
                        Nữ
                    </label>

                </div>

            </div>

            <div class="container_2">
                <label class="username_label" for="khoa">Phân khoa</label>
                <select name="khoa" id="khoa" class="phankhoa">
                    <?php
                    $phanKhoa = array(
                        "--Chọn phân khoa--",
                        "Khoa học máy tính",
                        "Khoa học vật liệu"
                    );
                    foreach ($phanKhoa as $pk) {
                        echo "<option value='" . $pk . "'>" . $pk . "</option>";
                    }
                    ?>
                </select>

            </div>

            <label class="username_label" for="date_of_birth">Ngày sinh</label>
            <input type="text" id="date_of_birth" class="phankhoa" placeholder="dd/mm/yyyy"><br><br>

            <label class="address_label" for="address">Địa chỉ</label>
            <textarea id="address" class="address"></textarea><br><br>

        

            <input type="submit" value="Đăng ký">
        </form>
        
        
    </div>

    <script>
        $(document).ready(function() {
            $('#registration-form').submit(function(e) {
                // Ngăn chặn hành vi mặc định của form
                e.preventDefault();

                // Xóa tất cả các thông báo lỗi hiện tại
                $('.error-message').remove();

                // Kiểm tra và hiển thị thông báo lỗi
                var errors = [];

                // Kiểm tra tên
                var name = $('#username').val().trim();
                if (name === "") {
                    errors.push("Hãy nhập tên.");
                }

                // Kiểm tra giới tính
                var gender = $('input[name="gioiTinh[]"]:checked').length;
                if (gender === 0) {
                    errors.push("Hãy chọn giới tính.");
                }

                // Kiểm tra phân khoa
                var major = $('#khoa').val();
                if (major === "--Chọn phân khoa--") {
                    errors.push("Hãy chọn phân khoa.");
                }

                // Kiểm tra ngày sinh
                var dateOfBirth = $('#date_of_birth').val().trim();
                if (dateOfBirth === "") {
                    errors.push("Hãy nhập ngày sinh.");
                } else {
                    var regexDate = /^\d{2}\/\d{2}\/\d{4}$/;
                    if (!regexDate.test(dateOfBirth)) {
                        errors.push("Hãy nhập ngày sinh đúng định dạng.");
                    }
                }

                // Hiển thị thông báo lỗi
                if (errors.length > 0) {
                    var errorList = '';
                    for (var i = 0; i < errors.length; i++) {
                        errorList += errors[i] + '<br>';
                    }
                    errorList += '<br>';
                    $('#error-container').html(errorList).addClass('errs');
            
                }
            });
        });
    </script>

</html>

