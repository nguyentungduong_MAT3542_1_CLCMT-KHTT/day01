<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <style>
     

        p {
            background-color: #f2f2f2;
            padding: 8px;
            width: 76%;
            margin-left: 1cm;
        }

        body {
            font-family: Arial, sans-serif;
            background-color: #f7f7f7;
            height: 100vh;
            margin: 0;

        }

        form {
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: left;
            max-width: 50%;
            border: 2px solid #007bff;
  

        }

        label {
            font-weight: bold;
            background-color: #007bff;
            padding: 10px;
            margin: 8px 0;
            color: white;
            display: inline-block;
            width: 30%;
            margin-right: 0.5cm;
            margin-left: 1cm;
            
        }
        
        #time-box {
            background-color: #e8e7e6;
            border-style: none;
            margin-left: 65px;
            margin-right: 65px;
        }


        input[type="text"] {
            width: 39%;
            padding: 10px;
            margin: 8px 0;
            border: 2px solid #007bff;
            text-align: center;
           

        }

        input[type="password"] {
            width: 39%;
            padding: 10px;
            margin: 8px 0;
            border: 2px solid #007bff;
            text-align: center;
        }


        input[type="submit"] {
            background-color: #007bff;
            color: #fff;
            padding: 10px 40px;
            border: none;
            border-radius: 5px;
            border-style;
            cursor: pointer;
            font-weight: bold;
            margin-top: 10px;
            text-align: center;
            border: 2px solid #007bff;
            display: block;
            margin: 0 auto;
        }
    </style>

</head>

<body>

    <?php
date_default_timezone_set('Asia/Ho_Chi_Minh');

$daysOfWeek = [
    1 => "thứ 2",
    2 => "thứ 3",
    3 => "thứ 4",
    4 => "thứ 5",
    5 => "thứ 6",
    6 => "thứ 7",
    7 => "Chủ nhật"
];
?>

<div>
    <h1></h1>
    <form action="process_login.php" method="POST">
<p id="time-box" ><?php echo "Bây giờ là: " . date("H:i:s") . ", " . $daysOfWeek[date("N")] . " ngày " .date("d/m/Y"); ?></p>



        <label for="username">Tên đăng nhập:</label>
        <input type="text" id="username" name="username" required><br><br>

        <label for="password">Mật khẩu:</label>
        <input type="password" id="password" name="password" required><br><br>

        <input type="submit" value="Đăng nhập">
    </form>
</div>

</html>