<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách tân sinh viên</title>
    <link rel="stylesheet" href="./style3.css">
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    
</head>

<body>
    <div class="container">
        <form id="searchForm">
            <label for="inputName" class="input_name">Khoa</label>
            <input type="text" name="inputName" id="inputName" class="entering" required><br><br>
        </form>
        <form id="keywordForm">
            <label for="inputKeyword" class="input_name">Từ Khóa</label>
            <input type="text" name="inputKeyword" id="inputKeyword" class="entering" required><br><br>
        </form>

        <form>
            <button class="button-container" id="resetButton" onclick="resetForm()"> Reset </button>
            <button class="button-container" id="nonsubmitButton" onclick="searchStudents()"> Tìm kiếm </button>
        </form>

        <form action="register.php">
            <button class="button-container" id="submitButton"> Thêm </button>
        </form>

        <p class="tittle">Số sinh viên tìm thấy : XXX</p>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="studentTableBody">
                    <?php
                    ini_set('display_errors', 1);
                    ini_set('display_startup_errors', 1);
                    error_reporting(E_ALL);

                    include 'database.php';

                    $sql = "SELECT name, department FROM STUDENTS";
                    $result = $conn->query($sql);

                    $i = 1;
                    while ($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $i . "</td>";
                        echo "<td>" . $row["name"] . "</td>";
                        echo "<td>" . $row["department"] . "</td>";
                        echo "<td>";
                        echo '<button class="button-container" id="tableButton">Xóa</button>';
                        echo '<button class="button-container" id="tableButton">Sửa</button>';
                        echo "</td>";
                        echo "</tr>";
                        $i++;
                    }

                    $conn->close();
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function resetForm() {
            document.getElementById("inputName").value = "";
            document.getElementById("inputKeyword").value = "";
            
            // Xóa nội dung của tbody
            $("#studentTableBody").empty();

            // Gọi hàm tìm kiếm để làm mới dữ liệu
            searchStudents();
        }


        function searchStudents() {
            var department = document.getElementById("inputName").value;
            var keyword = document.getElementById("inputKeyword").value;

            // Thực hiện AJAX để gửi yêu cầu tìm kiếm và xử lý kết quả ở đây
            $.ajax({
                url: 'search.php', // Đường dẫn đến file xử lý tìm kiếm
                method: 'POST',
                data: {
                    department: department,
                    keyword: keyword
                },
                success: function(response) {
                    // Cập nhật nội dung của tbody
                    $("#studentTableBody").html(response);
                }
            });
        }
    </script>
</body>

</html>
