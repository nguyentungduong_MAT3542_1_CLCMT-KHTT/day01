<?php
// search.php

include 'database.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Lấy dữ liệu từ yêu cầu AJAX
    $department = $_POST['department'];
    $keyword = $_POST['keyword'];

    // Xử lý tìm kiếm trong CSDL
    $sql = "SELECT name, department FROM STUDENTS WHERE department LIKE ? OR name LIKE ?";
    $stmt = $conn->prepare($sql);
    $searchTerm = "%$keyword%";
    $stmt->bind_param("ss", $department, $searchTerm);
    $stmt->execute();
    $result = $stmt->get_result();
    $stmt->close();

    // Tạo HTML cho kết quả tìm kiếm
    $i = 1;
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $i . "</td>";
        echo "<td>" . $row["name"] . "</td>";
        echo "<td>" . $row["department"] . "</td>";
        echo "<td>";
        echo '<button class="button-container" id="deleteButton">Xóa</button>';
        echo '<button class="button-container" id="editButton">Sửa</button>';
        echo "</td>";
        echo "</tr>";
        $i++;
    }
    
    // Đóng kết nối CSDL
    $conn->close();
}
?>
