<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận thông tin đăng ký</title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <div class="form_container">
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $full_name = $_POST["full_name"];
            $gender = $_POST["gender"];
            $dob_day = $_POST["dob_day"];
            $dob_month = $_POST["dob_month"];
            $dob_year = $_POST["dob_year"];
            $address = $_POST["district"] . ' - ' . $_POST["city"];
            $additional_info = $_POST["additional_info"];

            $dob = "$dob_day/$dob_month/$dob_year";

            echo "<p><strong>Họ và Tên</strong> $full_name</p>";
            echo "<p><strong>Giới tính</strong> $gender</p>";
            echo "<p><strong>Ngày sinh</strong> $dob</p>";
            echo "<p><strong>Địa chỉ</strong> $address</p>";
            echo "<p><strong>Thông tin khác</strong> $additional_info</p>";
        }
        ?>
    </div>

    
</body>

</html>
