var register_button = document.getElementById("register_button");
var full_name = document.getElementById("full_name");
var dob_day = document.getElementById("dob_day");
var dob_month = document.getElementById("dob_month");
var dob_year = document.getElementById("dob_year");
var gender_male = document.getElementById("gender_male");
var gender_female = document.getElementById("gender_female");
var dobValidation = document.getElementById("dob-validation");
var errorMessage = document.getElementById('error_messages');

errorMessage.innerHTML = '';

function loadDistricts() {
    var citySelect = document.getElementById("city");
    var districtSelect = document.getElementById("district");

    // Reset district options
    districtSelect.innerHTML = '<option value="" selected disabled>Quận</option>';

    if (citySelect.value === "Hà Nội") {
        // Load Hà Nội districts
        var hanoiDistricts = ["Ba Đình", "Hoàn Kiếm", "Hai Bà Trưng", "Đống Đa", "Tây Hồ", "Cầu Giấy"];
        hanoiDistricts.forEach(function(district) {
            var option = document.createElement("option");
            option.value = district;
            option.text = district;
            districtSelect.appendChild(option);
        });
    } else if (citySelect.value === "Hồ Chí Minh") {
        // Load Tp.Hồ Chí Minh districts
        var hcmcDistricts = ["Quận 1", "Quận 2", "Quận 3", "Quận 4", "Quận 5"];
        hcmcDistricts.forEach(function(district) {
            var option = document.createElement("option");
            option.value = district;
            option.text = district;
            districtSelect.appendChild(option);
        });
    }
}

var dobValidation = document.getElementById("dob-validation");

register_button.addEventListener("click", function(event) {
    event.preventDefault();

    // Validation
    var valid = true;

    if (full_name.value.trim() === "") {
        errorMessage.innerHTML += 'Hãy nhập họ tên.<br>';
        valid = false;
    } else {
        document.getElementById("fname-validation").innerHTML = "";
    }

    if (!gender_male.checked && !gender_female.checked) {
        errorMessage.innerHTML += 'Hãy chọn giới tính.<br>';
        valid = false;
    }

    if (dob_year.value === "" || dob_month.value === "" || dob_day.value === "") {
        errorMessage.innerHTML += "Hãy chọn ngày sinh.<br>";
        valid = false;
    } else {
        dobValidation.innerHTML = "";
    }

    var citySelect = document.getElementById("city");
    var districtSelect = document.getElementById("district");

    if (citySelect.value === "" || districtSelect.value === "") {
        errorMessage.innerHTML += "Hãy chọn địa chỉ."
        valid = false;
    } else {
        document.getElementById("address-validation").innerHTML = "";
    }

    if (valid) {
        // Proceed with form submission
        document.getElementById("registration_form").submit();
    }
});