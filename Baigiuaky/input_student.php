<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form đăng ký sinh viên</title>
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
    <h1>Form đăng ký sinh viên</h1>

    <div class="error_messages">
        <span id="error_messages"></span>
    </div>

    <br>

    <div class="form_container">
        <form id="registration_form" method="post" action="regist_student.php">
            <div class="form_section">
                <label for="full_name">Họ và Tên</label>
                <input type="name" id="full_name" name="full_name" required>
                <span id="fname-validation" class="error"></span>
            </div>

            <div class="form_section gender">
                <label>Giới tính</label>
                <input type="radio" id="gender_male" name="gender" value="Nam" required> Nam
                <input type="radio" id="gender_female" name="gender" value="Nữ" required> Nữ
            </div>

            <div class="form_section">
                <label for="dob">Ngày sinh</label>
                <select id="dob_year" name="dob_year" required>
                    <option value="" selected disabled>Năm</option>
                    <?php
                    $currentYear = date("Y");
                    for ($year = $currentYear - 40; $year <= $currentYear - 15; $year++) {
                    ?>
                        <option value="<?= $year ?>"><?= $year ?></option>
                    <?php } ?>
                </select>
                <select id="dob_month" name="dob_month" required>
                    <option value="" selected disabled>Tháng</option>
                    <?php for ($month = 1; $month <= 12; $month++) { ?>
                        <option value="<?= $month ?>"><?= str_pad($month, 2, '0', STR_PAD_LEFT) ?></option>
                    <?php } ?>
                </select>
                <select id="dob_day" name="dob_day" required>
                    <option value="" selected disabled>Ngày</option>
                    <?php for ($day = 1; $day <= 31; $day++) { ?>
                        <option value="<?= $day ?>"><?= str_pad($day, 2, '0', STR_PAD_LEFT) ?></option>
                    <?php } ?>
                </select>

                <span id="dob-validation" class="error"></span>
            </div>

            <div class="form_section">
                <label for="address">Địa chỉ</label>
                <select id="city" name="city" onchange="loadDistricts()">
                    <option value="" selected disabled>Thành phố</option>
                    <option value="Hà Nội">Hà Nội</option>
                    <option value="Hồ Chí Minh">Tp.Hồ Chí Minh</option>
                </select>
                <select id="district" name="district">
                    <option value="" selected disabled>Quận</option>
                </select>
                <span id="address-validation" class="error"></span>
            </div>

            <div class="form_section">
                <label>Thông tin khác</label>
                <textarea id="additional_info" name="additional_info"></textarea>
            </div>

            <div class="button_container">
                <button type="button" id="register_button">Đăng ký</button>
            </div>
        </form>

    </div>
</body>
<script src="validation.js"></script>
</html>
